# Basil Dsouza #
Software development happens to be my day job, but it is something I enjoy doing outside of work hours as well. 

I work on plenty of side projects to try out and get familiar with various technologies, but uploaded here are a selection of projects that I have written to solve specific use cases.

# Notable Projects #
## Optimus Pi ##
Optimus Pi / Wall-E is a Raspberry Pi based tracked robot that I had built with my son for his 7th birthday.

Video (Prototype build): https://youtu.be/srY3DXP5Sc4 / https://photos.app.goo.gl/KgnpJxXwTAW9XugNA

Video of final version: (with the RPi3A instead of RPiZero W, 5 point line sensor and much better cable management)

[![Optimus Pi - Final Build](https://img.youtube.com/vi/JetVJMWPOoQ/0.jpg)](https://www.youtube.com/watch?v=JetVJMWPOoQ)

Alternate link: https://photos.app.goo.gl/xkfxxe5LunUJ42My6

Check screenshots in the images directory: https://bitbucket.org/basild/optimus-pi-java/src/main/images/


### Java Version ###
The java version is based on Pi4j v2 integrated into spring boot and a vanilla js front end to allow for a first person view and control of the robot.
Screenshots are in the images folder in the repo

https://bitbucket.org/basild/optimus-pi-java/src/main/

### Python Version ###
https://bitbucket.org/basild/optimus-pi/src/main/optimuspi/

A Python based line follower: https://bitbucket.org/basild/optimus-pi/src/main/optimuspi/driver/local/line-driver.py

Video of it in action: https://photos.app.goo.gl/nHwuGNt2xrXHR1Kh9


## Hostile Harmonics ##
https://bitbucket.org/hmsgamedev/hostileharmonics/src/master/

Hostile Harmonics is a game / teaching tool to aid sight reading. It can connect to a piano via the midi ports and use that as an input to control various game elements.

[![Hostile Harmonics](https://img.youtube.com/vi/d3bU9_-TijY/2.jpg)](https://www.youtube.com/watch?v=d3bU9_-TijY)


## Songlist Creator ##
Songlist creator provides a way to create a prepared list of songs with their chords for an event.

https://bitbucket.org/basild/songlistcreator/src/master/

Hosted Link: 
[http://setlistcreator.basildsouza.com](http://setlistcreator.basildsouza.com)

## Inspector Parker - Auto Solver ##
Inspector parker is a puzzle game, and this program can solve it automatically.
Check the readme file in the project for screenshots and more information.

https://bitbucket.org/basild/inspector-parker/src/main/

## Text Untwist - Auto Solver ##
Text untwist is a word unscrambling game, and this tool can find the various combinations based on a dictionary, but also auto enter it into the game.
(See link below for screenshots, code and built files that can be run directly)

https://bitbucket.org/basild/text-untwister

# All of my uploaded projects #
https://bitbucket.org/basild/

https://github.com/basil-dsouza

